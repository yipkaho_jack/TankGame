using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyTankMovement : MonoBehaviour
{
    public Transform goal;
    public int lifePoint = 30;
    private UnityEngine.AI.NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.destination = goal.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
