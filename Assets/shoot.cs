using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    public Rigidbody prefabBullet;
    public float shootForce;
    public Transform shootPosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1")) {
            Rigidbody instanceBullet = (Rigidbody)Instantiate(prefabBullet, transform.position + transform.forward * 1.5F+ transform.up * 0.3F,shootPosition.rotation);
            instanceBullet.GetComponent<Rigidbody>().AddForce(shootPosition.forward * shootForce);
            }
    }
}
