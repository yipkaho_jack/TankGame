using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class bossAI : MonoBehaviour
{   
    public enum TankType{Boss, Normal}
    public TankType EnemyType;
    Transform player;
    //time
    private int count= 0;

    private float state1dt= 0.0F;
    
    //hp
    public int lifePoint = 300;
    //mode
    public float scanRange = 30.0F;
    private int state;
    public float rotationDamping = 6.0f;
    public Rigidbody prefabBullet;
    public float shootForce;
    public Transform shootPosition;
    //Patrol
    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Tank").transform;
        agent = GetComponent<NavMeshAgent>();
            // Disabling auto-braking allows for continuous movement
            // between points (ie, the agent doesn't slow down as it
            // approaches a destination point).
        agent.autoBraking = false;
        GotoNextPoint();
        StartCoroutine("attackOrMove");
    }
    void GotoNextPoint() 
        {
            // Returns if no points have been set up
            if (points.Length == 0)
                return;

            // Set the agent to go to the currently selected destination.
            agent.destination = points[destPoint].position;

            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            destPoint = (destPoint + 1) % points.Length;
        }
    // Update is called once per frame
    void Update()
    {
        GameObject enemy;
        Vector3 heading;
        enemy = GameObject.FindGameObjectWithTag("Player");
        heading = enemy.transform.position - transform.position;
        if (state == 0) 
        {   GameObject.Find("rs2").transform.localRotation= Quaternion.Euler(0.0F, 0.0F, 0.0F);
            GameObject.Find("rs1").transform.localRotation= Quaternion.Euler(0.0F, 0.0F, 0.0F);
            if (!agent.pathPending && agent.remainingDistance < 0.5f)
                GotoNextPoint();
            if (infront() && isLineOfsight())
            {
                state = 1;
                agent.isStopped = true;
            } 
        }
            else if(state == 1)
            {   
            Quaternion rotation = Quaternion.LookRotation(enemy.transform.position -transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
            
            GameObject.Find("rs2").transform.localRotation= Quaternion.Euler(0.0F, 12.0F*state1dt, 0.0F);
            GameObject.Find("rs1").transform.localRotation= Quaternion.Euler(0.0F, -12.0F*state1dt, 0.0F);
            if(state1dt<5.0F)
            state1dt+=Time.deltaTime;
            else
            state1dt = 5.0F;
            }  
                else if(state == 2) 
                {
                    Quaternion rotation = Quaternion.LookRotation(enemy.transform.position -transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);

            /*if (!infront() || !isLineOfsight())
            {
                state = 0;
                agent.isStopped = false;
            } */
                }
                    else if(state == 3)
                    {   
                        GameObject.Find("rs2").transform.localRotation= Quaternion.Euler(0.0F, 12.0F*state1dt, 0.0F);
                        GameObject.Find("rs1").transform.localRotation= Quaternion.Euler(0.0F, -12.0F*state1dt, 0.0F);
                        if(state1dt>0)
                        state1dt-=Time.deltaTime;
                        else
                        state1dt=0.0F;
                    }

    }

    bool infront()
    {
        Vector3 directionOfPlayer = transform.position - player.position;
        float angle = Vector3.Angle(transform.forward,directionOfPlayer);

        if(Mathf.Abs(angle) > 120 && Mathf.Abs(angle) < 240)
        {
            Debug.DrawLine(transform.position,player.position,Color.red);
            return true;
        }

        return false;
    }

    bool isLineOfsight()
    {
        RaycastHit hit;
        Vector3 directionOfPlayer = player.position -transform.position;

        if(Physics.Raycast(transform.position, directionOfPlayer ,out hit,scanRange))
        {
            if(hit.transform.name == "Tank")
                {
                    Debug.DrawLine(transform.position,player.position,Color.green);
                    return true;
                }
        }

        return false;
    }
    
    private void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.collider.tag == "Shell")
            {
                lifePoint -= 10;
                if (lifePoint <= 0) 
                    Destroy(gameObject, 0.5F);
            }
    } 
    IEnumerator attackOrMove()
        {   
            GameObject enemy;
            while (true) 
            {
                if (state == 0) 
                {
                yield return new WaitForSeconds(1.0f);
                }
                else if(state == 1)
                    {   
                        Debug.Log(state);
                        if(count != 0)
                        {
                        state = 2;
                        count = 0;
                        }
                        if(state != 2)
                        count++;
                        else
                        count = 0;
                        yield return new WaitForSeconds(1.0f);
                    }
                    else if(state == 2)
                        {   Debug.Log(state);
                            enemy = GameObject.FindWithTag("Player");
                            transform.LookAt(enemy.transform.position);
                            Rigidbody instanceBullet = Instantiate(prefabBullet, shootPosition.position + shootPosition.forward * 2.5f, shootPosition.rotation);
                            instanceBullet.GetComponent< Rigidbody >().AddForce(shootPosition.forward * shootForce);
                            state = 3;
                            yield return new WaitForSeconds(2.0f);
                        }
                        else if(state == 3)
                        {   
                            Debug.Log(state);
                            Debug.Log("count:"+count);
                            if(count != 0)
                            {   
                                state = 1;
                                count = 0;
                            }
                            Debug.Log("count:"+count);
                            if(state != 1)
                                count++;
                            else
                                count = 0;
                            enemy = GameObject.FindWithTag("Player");
                            transform.LookAt(enemy.transform.position);
                            yield return new WaitForSeconds(10.0f);
                        }
            }
        }         
        
}
